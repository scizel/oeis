__author__ = 'Rok_in_Sebastjan '

import requests
import re
from collections import OrderedDict
import html

#zaporedje=1,3,6,10,15

def boljsi_poberi_kodo_zaporedja1(zaporedje):
    """
    Funkcija sprejme zaporedje, kot ga vnese uporabnik in vrne "slabo", če ni matchov oz. "preveč", če je
    preveč matchov - torej polovi slabe primere, pri katerih iskalnik ne vrne zaporedij.

    """
    DATA=dict(q=str(zaporedje))
    poskus_posta=requests.post("https://oeis.org/",data=DATA).text
    if re.findall(r"Sorry, but the terms do not match anything in the table", poskus_posta):
        return "slabo"
    if re.findall(r"too many to show. Please refine your search.", poskus_posta):
        return "preveč"
    # links=re.findall(r"<a href=\"/A(\d*)\">A(\d*)</a>", poskus_posta) #ker sta 2 grupi, bo vračalo 2-tuple oblike (a,a), ker so entryji isti
    # nov=[]
    # iskalni_niz=[]
    # for element in links:
    #     #print(element[0]) #samo za mene, da vidim če je okej
    #     nov.append(element[0]) #samo enega zares rabimo
    #     # torej dobimo seznam nov, ki vsebuje vse tuple
    # for i in range(len(nov)):
    #     iskalni_niz.append("A"+str(nov[i]))
    # return iskalni_niz # s tem delom bi dejansko lahko iskali tudi kodo zaporedja

# s=requests.Session()
#
# def poberi_nakljucno():
#     poskus_posta=s.get("http://oeis.org/webcam").text
#     print(poskus_posta)
#     links=re.findall(r"<a href=\"/A(\d*)\">A(\d*)</a>", poskus_posta) #ker sta 2 grupi, bo vračalo 2-tuple oblike (a,a), ker so entryji isti
#     print(links)
#     nov=[]
#     iskalni_niz=[]
#     for element in links:
#        #print(element[0]) #samo za mene, da vidim če je okej
#         nov.append(element[0]) #samo enega zares rabimo
#         # torej dobimo seznam nov, ki vsebuje vse tuple
#     for i in range(len(nov)):
#         iskalni_niz.append("A"+str(nov[i]))
#     return iskalni_niz


def naredi_vse(zaporedje):

    def boljsi_poberi_kodo_zaporedja(zaporedje):
        """
        Funkcija sprejme iskalni niz (zaporedje) in vrne seznam, v katerem je prvi element koda zaporedja, ki se
        z iskalnim nizom najbolj ujema, ostali elementi pa so kode sorodnih zaporedij iskanega zaporedja
        """
        DATA=dict(q=str(zaporedje))
        poskus_posta=requests.post("https://oeis.org/",data=DATA).text
        links=re.findall(r"<a href=\"/A(\d*)\">A(\d*)</a>", poskus_posta) #ker sta 2 grupi, bo vračalo 2-tuple oblike (a,a), ker so entryji isti
        nov=[]
        iskalni_niz=[]
        for element in links:
            #print(element[0]) #samo za mene, da vidim če je okej
            nov.append(element[0]) #samo enega zares rabimo
            # torej dobimo seznam nov, ki vsebuje vse tuple
        for i in range(len(nov)):
            iskalni_niz.append("A"+str(nov[i]))
        return iskalni_niz


    iskana=boljsi_poberi_kodo_zaporedja(zaporedje)[0]
    ostale=boljsi_poberi_kodo_zaporedja(zaporedje)[1:]
    koda_zaporedja=iskana


    s=requests.Session() #to morda ni treba tukaj pisat
    tekst = s.get("https://oeis.org/"+koda_zaporedja).text

    def poberi_ime():
        """
        Funkcija pobere ime zaporedja
        """
        #s=requests.Session() #to morda ni treba tukaj pisat
        #tekst = s.get("https://oeis.org/"+koda_zaporedja).text
        #print(tekst)
        #ime=re.findall(r"<td(.*\s*)align=\"left\"(.*\s*)>(.*)<", tekst)
        #ime=re.findall(r"<td(.*\s*)*?>(.*)<br></br>", tekst)
        ime=re.findall(r"<td valign=top align=left>(.*\s*)*?<br", tekst)
        #print(ime)
        nov=[]
        for element in ime:
            element=element.strip()
            nov.append(element)
        return nov[0] # poglej če je to dobro, če ne pač konkateniramo


    def poberi_clene():
        """
        Funkcija pobere prvih nekaj členov zaporedja
        """
        #s=requests.Session() #to morda ni treba tukaj pisat
        #tekst = s.get("https://oeis.org/"+koda_zaporedja).text
        ime=re.findall(r"<tt>(.*)</tt", tekst)
        pravo_ime=ime[0]
        return pravo_ime

    def poberi_reference():
        """
        Funkcija pobere reference
        """
        #s=requests.Session() #to morda ni treba tukaj pisat
        #tekst = s.get("https://oeis.org/"+koda_zaporedja).text
        #referenca=re.findall(r"REFERENCES(.*\n*)*LINKS", tekst, re.MULTILINE)
        referenca=re.search(r"<tr>(.*\s*){3}.*REFERENCES(.*\s*){2}(.*(<tt>(.*)</tt>\s*))*<tr>(.*\s*){3}.*[A-Z][A-Z]+", tekst) #da dobim celotni tekst med references in naslednjim razdelkom
        referenca=referenca.group(0) #group(0) je celotna stvar ki jo vrne search. Search vrne objekt
        referenca=html.unescape(referenca)
        ref=re.findall(r"<tt>(.*)</tt>",referenca) #zdaj pa iščemo vsako referenco posebej
        # s=[] tako bi se "manualno" popravljalo kodo in vn metalo html oznake
        # for referen in ref:
        #     if "&quot;" in referen:
        #         novo=re.search(r"&quot;(.*)&quot;", referen)
        #         m=novo.group(1)
        #         referen=re.sub(r"&quot;(.*)&quot;", "\""+m+"\"", referen)
        #     s.append(referen)
        # ref=s
        stikalo=1
        while stikalo>0:
            stikalo=0
            s=[]
            for referen in ref:
                if "<a href=" in referen:
                    stikalo=1
                    novo=re.search(r"<a(.*)>(.*)</a>", referen)
                    m=novo.group(2)
                    n=novo.group(0)
                    referen=re.sub(n,m,referen)
                s.append(referen)
            ref=s
        konkateniraj=ref[0]
        for entry in ref[1:]:
            konkateniraj+="\n"+entry
        return konkateniraj



    def poberi_mathematico():
        """
        Funkcija pobere kodo za Mathematico
        """
        #s=requests.Session() #to morda ni treba tukaj pisat
        #tekst = s.get("https://oeis.org/"+koda_zaporedja).text
        #referenca=re.findall(r"REFERENCES(.*\n*)*LINKS", tekst, re.MULTILINE)
        referenca=re.search(r"<tr>(.*\s*){3}.*MATHEMATICA(.*\s*){2}(.*(<tt>(.*)</tt>\s*))*<tr>(.*\s*){3}.*[A-Z][A-Z]+", tekst) #da dobim celotni tekst med references in naslednjim razdelkom
        referenca=referenca.group(0) #group(0) je celotna stvar ki jo vrne search. Search vrne objekt
        referenca=html.unescape(referenca)
        ref=re.findall(r"<tt>(.*)</tt>",referenca) #zdaj pa iščemo vsako referenco posebej
        #print(ref)

        #je to kul, da so polek linki ?

        nov=[]
        for element in ref:
            if "(*" in element:
                refer=re.search(r"(.*?)\(\*", element)
                nov.append(refer.group(0)[:-2])
            else:
                nov.append(element)
        #print(nov)
        konkateniraj=nov[0]
        for entry in nov[1:]:
            konkateniraj+="\n"+entry
        return konkateniraj

    slovar={"Ime zaporedja": poberi_ime()}
    slovar=OrderedDict(slovar)
    slovar.update({"Členi zaporedja":poberi_clene()})
    slovar.update({"Reference":poberi_reference()})
    slovar.update({"Koda za Mathematico":poberi_mathematico()})
    slovar.update({"Druga zaporedja":ostale})
    #, "Členi zaporedja":poberi_clene(), "Reference":poberi_reference(), "Koda za Mathematico":poberi_mathematico(), "Sorodna zaporedja":ostale}
    return slovar

#print(naredi_vse(zaporedje))

def dolzina(vrst,w=40):
    """
    Funkcija uredi besedilo vrst tako, da je v vsaki vrstici največ 40 znkov
    """
    vrst=vrst.strip()
    if len(vrst)<=w:
        return vrst
    else:
        vr=vrst.strip().split(" ")
        if len(vr)==1:
            return vr[0]
        elif len(vr[0])>w:
            m=len(vr[0])
            return vr[0]+dolzina(vr[m:], w)
        else:
            a=vr[0].strip()
            i=len(a)
            beseda=a
            for element in vr[1:]:
                    element=element.strip()
                    m=len(element)
                    if len(element)==0:
                        continue
                    if i+m<w:
                        beseda+=" "+element
                        i+=m+1
        beseda=beseda.strip()
        m=len(beseda)
        return beseda+"\n"+dolzina(vrst[m:],w)

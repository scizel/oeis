__author__ = 'Rok_in_Sebastjan '

from Paketi import *
from Havlas_funkcije import boljsi_poberi_kodo_zaporedja1



class Zacetno():

    def iskanje(self, *args):
        '''
        Funkcija preveri pravilnost vpisa in ustreznost podatkov iz spletne strani, ter v ustreznem primeru začne spremembo
        okna
        '''

        if self.vnos.get() == "":
            messagebox.showwarning("Napaka", "Niste vpisali zaporedja!")

        odgovor = boljsi_poberi_kodo_zaporedja1(self.vnos.get())

        if odgovor == "slabo":
                messagebox.showwarning("Napaka", "Ni bilo mogoče najti nobenega zaporedja.")
        elif odgovor == "preveč" :
                messagebox.showwarning("Napaka", "Iskanje vrne preveliko število rezultatov. Prosim, omeji iskanje.")
        else:
            self.vnos = self.vnos.get()
            self.zamenjaj = True



    def __init__(self, root):

        root.title("Iskanje zaporedja")


        #
        #   Definicija atributov začetnega okna, ki bodo signalizirala spremembo in prenesla uporabnikov vnos v
        #   glavno okno.
        #

        self.zamenjaj = False
        self.vnos = StringVar()

        #
        #   Osnovna postavitev
        #

        zgOkv = Frame(root, padx = 5, pady = 5)
        srOkv = Frame(root, padx = 5, pady = 5)
        spOkv = Frame(root, padx = 5, pady = 5)
        zgOkv.pack(side = TOP, fill = BOTH, expand = True)
        spOkv.pack(side = BOTTOM, fill = BOTH, expand = True)
        srOkv.pack(fill = BOTH, expand = True)
        zgOkv.configure(background = barva_ozd)
        spOkv.configure(background = barva_ozd)
        srOkv.configure(background = barva_ozd)

        #
        #   Naslov in navodilo
        #

        txt = Label(srOkv, text = navodilo, font = ("Helvetica", 16), bg = barva_ozd)
        title = Label(zgOkv, text = naslov, font = ("Helvetica", 22), bg = barva_ozd)
        txt.pack(side = TOP)
        title.pack()

        #
        #   Vnosna vrstica
        #

        vnosno = Entry(srOkv, textvariable = self.vnos)
        vnosno.focus()
        vnosno.pack(side = BOTTOM, expand = True, fill = BOTH)


        self.isci = ttk.Button(spOkv, text = "Išči", command = self.iskanje)

        self.isci.pack()

        root.bind('<Return>', self.iskanje)
        txt.pack(side = TOP)
        vnosno.pack(side = BOTTOM, expand = True, fill = BOTH)
        title.pack()

